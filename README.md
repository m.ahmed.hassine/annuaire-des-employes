This CRUD Application is created using Angular for Front End and Spring Boot for Back End.
This is the properties file :
spring.datasource.url=jdbc:mysql://localhost:3306/db_employees?serverTimezone=UTC
spring.datasource.username=root
spring.datasource.password=root

spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5InnoDBDialect

spring.jpa.hibernate.ddl-auto = update